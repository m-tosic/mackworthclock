import { Schema, model } from 'mongoose';

const TestSchema = Schema({
  userId: String,
  startAt: Date,
  jumps: [{
    at: Number,
    from: Number,
    to: Number,
    reactionTime: Number
  }],
  reactions: [{
    correct: Boolean,
    dotPosition: Number,
    time: Number,
    isRelatedToJump: Boolean
  }]
});

export const Test = model('Test', TestSchema);

export function getAllTests(callback) {
  Test.find(callback);
}

export function addTest(newTest, callback) {
  newTest.save(callback);
}

export function deleteTestById(id, callback) {
  let query = { _id: id };
  Test.remove(query, callback);
}
