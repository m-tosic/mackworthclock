import express from 'express';
import { join } from 'path';
import { urlencoded, json } from 'body-parser';
import cors from 'cors';
import { connect } from 'mongoose';
import { database } from './config/database';
import test from './controllers/TestController';

connect(database);

const port = process.env.PORT || 3000;
const app = express();

app.use(urlencoded({ extended: true }));
app.use(json());
app.use(cors());
app.use(express.static(join(__dirname, 'public')));
app.use('/test', test);

app.get('/*', (req, res) => {
  res.sendFile(join(__dirname, 'public/index.html'));
});

app.listen(port, () => {
  console.log(`Starting the server at port ${port}`);
});
