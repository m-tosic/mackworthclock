import { Router } from 'express';
import { Test, getAllTests, addTest, deleteTestById } from '../models/Test';

const router = Router();

router.get('/', (req, res) => {
  getAllTests((err, tests) => {
    if (err) {
      res.json({ success: false, message: `Failed to load all tests. Error: ${err}` });
    } else {
      res.write(JSON.stringify({ success: true, tests: tests }, null, 2));
      res.end();
    }
  });
});

router.post('/post/', (req, res, next) => {
  console.log(req.body);
  let newTest = new Test({
    userId: req.body.userId,
    startAt: req.body.startAt,
    jumps: req.body.jumps,
    reactions: req.body.reactions
  });
  addTest(newTest, (err, test) => {
    if (err) {
      res.json({ success: false, message: `Failed to create a new test. Error: ${err}` });
    } else {
      res.json({ success: true, message: "Added successfully." });
    }
  });
});

router.delete('/:id', (req, res, next) => {
  let id = req.params.id;
  console.log(id);
  deleteTestById(id, (err, test) => {
    if (err) {
      res.json({ success: false, message: `Failed to delete the test. Error: ${err}` });
    } else if (test) {
      res.json({ success: true, message: "Deleted successfully" });
    }else {
      res.json({ success: false });
    }
  })
});

export default router;
