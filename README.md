# MackworthClock

The implementation of a web-based version of the Mackworth Clock task described in Martel, Dähne, & Blankertz (2014)

## Technology

This project was created using Angular 6 for frontend, node.js for backend with mongoDB for storing data.

## Requirements

You should have [node.js](https://nodejs.org/en/download/) and [mongoDB](https://docs.mongodb.com/manual/installation/) installed.

## Running the project

Make sure mongoDB process is running in the background.

In the root folder run `npm install` and then `npm start`.

In ./frontend folder run `npm install` and then `ng serve`.

Open [localhost:4200](localhost:4200) to use the application.

## Author

Author Mateo Tošić

Student @ Eberhard Karls Universität Tübingen

