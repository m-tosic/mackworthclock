import { Component, HostListener } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';

import { Test } from '../../../../models/Test.js';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent {
  numberOfItems: number = 42;
  interval: number = 1000;
  duration: number = 15 * 60;
  testCompleted: boolean;

  constructor(private http: HttpClient) { }

  @HostListener('window:beforeunload', ['$event'])
  onbeforeunload() {
    if (!this.testCompleted) {
      return false;
    }
  }

  onClockDone(event) {
    this.testCompleted = true;
    console.log(event.log, event.test);
    const body = JSON.stringify(event.test);
    this.addTest(event.test).subscribe((res) => console.log(res));
    console.log(body);
  }

  addTest(test: Test): Observable<Test> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    return this.http.post<Test>('https://mackworthclock.herokuapp.com/test/post/', test, { headers: headers })
      .pipe(catchError(this.handleError));
  }

  private handleError(error: HttpErrorResponse) {
    console.error(error);
    return throwError('error');
  }
}
