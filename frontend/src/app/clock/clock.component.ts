import { Component, Input, Output, EventEmitter, HostListener } from '@angular/core';
import { Test } from '../../../../models/Test.js';

@Component({
  selector: 'clock',
  templateUrl: './clock.component.html',
  styleUrls: ['./clock.component.scss']
})
export class ClockComponent {
  @Input() numberOfItems: number;
  @Input() duration: number;
  @Input() interval: number;

  @Output() onClockDone = new EventEmitter<any>();

  isRunning: boolean;
  itemIds: number[];
  activeItemId: number;
  wrongReactionItemId: number;
  timeElapsed: number;
  jumps: number[];
  lastJumpTimeStamp: Date;
  shouldPressSpace: boolean;
  test: Test = {
    jumps: [],
    reactions: []
  };
  intervalBounds: number[];
  isLeftDistractionVisible: boolean;
  isRightDistractionVisible: boolean;
  distractionImageId = 0;
  distractionImageUrl = 'http://lorempixel.com/296/396/food/';
  log: string = '---------------START----------------\n';

  constructor() { }

  start(userId: string) {
    this.writeToLog(`User ID: ${userId}\n`);
    this.test.userId = userId;
    this.isRunning = true;
    this.itemIds = Array.from(Array(this.numberOfItems).keys());
    this.activeItemId = 0;
    this.timeElapsed = 0;

    this.generateJumps();

    this.generateDistractionIntervals();

    this.test.startAt = new Date();

    let intervalId = setInterval(() => {
      if (this.timeElapsed >= this.duration) {
        this.onClockDone.emit({ log: this.log, test: this.test });
        clearInterval(intervalId);
      }

      this.wrongReactionItemId = this.shouldPressSpace ? this.activeItemId : -1;

      if (this.shouldPressSpace) {
        let reaction = {
          correct: false,
          dotPosition: this.activeItemId,
          time: -1,
          isRelatedToJump: true
        };
        this.test.reactions.push(reaction);
        this.writeToLog(`No reaction to jump at ${this.timeElapsed}`);
      }

      this.timeElapsed += 1;

      let jump = {
        at: this.timeElapsed,
        from: this.activeItemId,
        to: -1,
        reactionTime: -1
      }
      
      if (this.jumps.find(n => n >= this.timeElapsed) === this.timeElapsed) {
        this.writeToLog(`Jump from ${this.activeItemId}`);
        this.activeItemId++;
        this.shouldPressSpace = true;
      } else {
        this.writeToLog(`Movement from ${this.activeItemId}`);
        this.shouldPressSpace = false;
      }

      this.activeItemId = (this.activeItemId + 1) % this.itemIds.length;

      this.lastJumpTimeStamp = new Date();

      jump.to = this.activeItemId;

      this.test.jumps.push(jump);

      this.writeToLog(` to ${this.activeItemId} at ${Math.floor(this.timeElapsed / 60)}` +
        `:${this.timeElapsed % 60 > 9 ? '' : 0}${this.timeElapsed % 60}\n`);

      if (this.intervalBounds.find(n => n >= this.timeElapsed) === this.timeElapsed) {
        this.distractionImageId++;
        this.distractionImageUrl = `http://lorempixel.com/${296+this.distractionImageId}/${396+this.distractionImageId}/food/`;
        this.isLeftDistractionVisible = !this.isLeftDistractionVisible && this.timeElapsed % 2 === 0;
        this.isRightDistractionVisible = !this.isRightDistractionVisible && this.timeElapsed % 2 != 0;
      }
    }, this.interval);
  }

  @HostListener('document:keypress', ['$event'])
  onKeypress(event: KeyboardEvent) {
    if (event.code === 'Space') {
      this.wrongReactionItemId = this.shouldPressSpace ? - 1 : this.activeItemId;
      let reactionTime: number = new Date().getTime() - this.lastJumpTimeStamp.getTime();
      let reaction = {
        correct: this.shouldPressSpace,
        dotPosition: this.activeItemId,
        time: reactionTime,
        isRelatedToJump: this.shouldPressSpace
      };
      this.writeToLog(`${this.shouldPressSpace ? 'C' : 'Inc'}orrect reaction in ${reactionTime} milliseconds\n`);
      this.shouldPressSpace = false;
      this.test.reactions.push(reaction);
      this.test.jumps[this.test.jumps.length - 1].reactionTime = reactionTime;
    }
  }

  generateJumps() {
    let jumpsSet = new Set();
    while (jumpsSet.size < this.duration * 0.1) {
      console.log(jumpsSet);
      jumpsSet.add(Math.floor(Math.random() * this.duration));
    }
    this.jumps = Array.from(jumpsSet);
    this.jumps.sort((a, b) => a - b);
    this.writeToLog(`Jumps: ${this.jumps.toString()}\n`);
  }

  generateDistractionIntervals(): any {
    let intervalBoundsSet = new Set();
    while (intervalBoundsSet.size < 16) {
      intervalBoundsSet.add(Math.floor(Math.random() * this.duration));
    }
    this.intervalBounds = Array.from(intervalBoundsSet);
    this.intervalBounds.sort((a, b) => a - b);
    console.log(this.intervalBounds);
    for (let i = 0; i < this.intervalBounds.length - 1; i++) {
      this.intervalBounds[i + 1] = Math.max(this.intervalBounds[i + 1], this.intervalBounds[i] + 10);
    }
    console.log(this.intervalBounds);
  }

  writeToLog(text: string) {
    this.log += text;
    console.log(text);
  }
}
