import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Test } from '../../../../models/Test.js';
import { Chart } from 'chart.js';

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.scss']
})
export class ResultsComponent {
  isAdmin: boolean;
  tests: Test[];
  selectedTest: Test;
  selectedTestId: number;
  chart: Chart;

  constructor(private http: HttpClient) { }

  login(password: string) {
    this.isAdmin = password === '6,626x10-24m2kg/s';

    if (!this.isAdmin) {
      alert('Incorrect password');
    } else {
      this.http.get('https://mackworthclock.herokuapp.com/test').subscribe((data: Test[]) => {
        this.tests = data['tests'];
        console.log(this.tests);
        this.selectedTest = this.tests[0];
        this.selectedTestId = 0;
      });
    }
  }

  selectTest(id: number) {
    this.selectedTestId = id;
    this.selectedTest = this.tests[id];
  }

  downloadResults() {
    let csv: string = '"User","Start at","Reaction correct","Reaction time (ms)","Dot position","Related to a jump"\r\n';
    this.tests.forEach(test => {
      let testCsv: string = '';
      for (let i = 0; i < test.reactions.length; i++) {
        let reaction = test.reactions[i];
        testCsv += `"${test.userId}","${test.startAt.toLocaleString()}","${reaction.correct}",`;
        testCsv += `"${reaction.time}","${reaction.dotPosition}","${reaction.isRelatedToJump}"\r\n`;
      }
      csv += testCsv;
    });
    let fileName: string = `tests${new Date().toLocaleDateString()}.csv`;

    let blob: Blob = new Blob([csv], { type: 'text/csv;charset=utf-8;' });
    if (navigator.msSaveBlob) { // IE 10+
        navigator.msSaveBlob(blob, fileName);
    } else {
        let link: HTMLAnchorElement = document.createElement('a');
        if (link.download !== undefined) {
            let url: string = URL.createObjectURL(blob);
            link.setAttribute('href', url);
            link.setAttribute('download', fileName);
            link.style.visibility = 'hidden';
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
        }
    }
  }
}
